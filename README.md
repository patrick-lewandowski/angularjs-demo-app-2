# AngularJS Demo App 2

Demo application to test out AngularJS 1.5.x.

## Commands

`npm start`

* starts a local server on [http://localhost:8000](http://localhost:8000)

## License

[MIT](LICENSE)