'use strict';

angular.
  module('sideNav').
  component('sideNav', {
    templateUrl: 'side-nav/side-nav.template.html',
    controller: ['$http',
      function SideNavController($http) {
        const self = this;
        self.state = false;

        self.toggleState = () => {
          self.state = !self.state;
        };

        $http.get('data/items.json').then((response) => {
          self.items = response.data;
        });
      }
    ]
  });
