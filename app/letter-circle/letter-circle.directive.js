'use strict';

angular.
  module('letterCircle').
  directive('letterCircle', () => {
    return {
      restrict: 'E',
      scope: {
        letter: '@'
      },
      templateUrl: 'letter-circle/letter-circle.template.html'
    };
  });
