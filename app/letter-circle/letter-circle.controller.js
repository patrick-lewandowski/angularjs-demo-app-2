'use strict';

angular.
  module('letterCircle').
  controller('letterCircleController', ['$scope', ($scope) => {
    $scope.size = 32;
    $scope.changeSize = () => {
      if ($scope.size >= 32)
        $scope.size = 12;
      else
        $scope.size += 4;
    }
  }]);
